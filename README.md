# Embedded ApacheDS

**Example project for an embedded [ApacheDS][apacheds] LDAP server.** 


--------------------


[TOC]


The project contains mainly of three parts: 


## 1. Embedded ApacheDS LDAP server

A server which is started when the application is run and which is applied with some test data (basically three users *admin*, *user* and *ttester*). The server will be available at localhost:10389 without usernam/password (e.g. you could connect to it with Apache Directory Studio to browse schema and data).

 
## 2. JNDI LDAP connector

Simply connecting to the LDAP server via JNDI and executing a simple search on the attribute *'uid'*.

No magic here.


## 3. Unit test

Rather interesting is the unit test, I created for the connector, which starts an LDAP server including dummy data to run the tests against.

* `@RunWith(FrameworkRunner.class)` - Run unit test with ApacheDS FrameworkRunner
* `@CreateDS` - Create directory service according to attributes
* `@CreateLdapServer` - Creates the actual LDAP server
* `@ApplyLdifFiles` - Applies one or more LDIF files to the running server instance
* `@ApplyLdifs` - Applies LDIF commands, *but never really worked for me*


--------------------

# Program execution

`java -jar apacheds-0.1.jar`

Example output:

```
Initializing embedded LDAP server
Embedded LDAP server started

Enter username to search LDAP for: test
Searching for 'test' ...

Enter username to search LDAP for: admin
Searching for 'admin' ...
Found:
   cn=Admin Istrator+sn=Istrator+uid=admin,ou=ABC,o=TEST

Enter username to search LDAP for: sdf
Searching for 'sdf' ...

Enter username to search LDAP for: ttester
Searching for 'ttester' ...
Found:
   cn=Tim Tester+uid=ttester,ou=XYZ,o=TEST

Enter username to search LDAP for: 
Embedded LDAP server stopped
```


--------------------


# Things to know or questions I frequently asked myself

## Why are you excluding jdbm:apacheds-jdbm1 only to re-include it later?

That's easy: There's an unresolvable artifact in all versions higher than 2.0.0-M5.

`org.apache.directory.jdbm:apacheds-jdbm1:bundle:2.0.0-M2` 

The newer version of the artifact is `org.apache.directory.jdbm:apacheds-jdbm1:bundle:2.0.0-M3` but one of the
apacheDS pom files still point to the missing dependency.

The inclusion of `org.apache.directory.jdbm:apacheds-jdbm1:bundle:2.0.0-M3` also requires the use of apache's 
maven repository https://repository.apache.org/content/repositories/snapshots.


## Why can't I run the LDAPTest in my IDE? I got an error.

Does the error look like this:

```
2013-05-22 20:49:14 ERROR FrameworkRunner:285 - ERR_181 Failed to run the class com.pirateninjaunicorn.examples.apacheds.client.LdapTest
2013-05-22 20:49:14 ERROR FrameworkRunner:286 - Problem locating LDIF file in schema repository
Multiple copies of resource named 'schema/ou=schema/cn=apachemeta/ou=matchingrules/m-oid=1.3.6.1.4.1.18060.0.4.0.1.3.ldif' located on classpath at urls
    jar:file:/opt/maven-repository/org/apache/directory/server/apacheds-all/2.0.0-M5/apacheds-all-2.0.0-M5.jar!/schema/ou%3dschema/cn%3dapachemeta/ou%3dmatchingrules/m-oid%3d1.3.6.1.4.1.18060.0.4.0.1.3.ldif
    jar:file:/opt/maven-repository/org/apache/directory/shared/shared-ldap-schema-data/1.0.0-M10/shared-ldap-schema-data-1.0.0-M10.jar!/schema/ou%3dschema/cn%3dapachemeta/ou%3dmatchingrules/m-oid%3d1.3.6.1.4.1.18060.0.4.0.1.3.ldif
```

This is a bug mentioned on [DIRSERVER-1606][issue] and excluding the libraries (`api-ldap-schema-data`, `shared-ldap-schema` and/or `shared-ldap-schema-data`) worked for me and the tests were running and everybody was happy.

**Except** I couldn't exclude the libraries for a console application, which needed all dependencies bundled in a single JAR. So there we are ... I included them again and just excluded them for the Maven Surefire plugin. I hate it, but it worked.

But if you don't have to worry about that, just exclude the libraries from the pom.xml alltogether, that's an easy way to go.


--------------------


# Links

Although the documentation for ApacheDS 2.0.0 is really scarce, the following links helped me figuring out the API usage.  

* [Embedding ApacheDS into an application][application]
* [Using ApacheDS for unit tests][unittest]
* [Adding your own partition/suffix][partition]


[apacheds]: http://directory.apache.org/apacheds/ "ApacheDS"

[issue]: https://issues.apache.org/jira/browse/DIRSERVER-1606 "DIRSERVER-1606"

[application]: https://cwiki.apache.org/DIRxSRVx11/41-embedding-apacheds-into-an-application.html "Embedding ApacheDS into an application"

[unittest]: https://cwiki.apache.org/DIRxSRVx11/42-using-apacheds-for-unit-tests.html "Using ApacheDS for unit tests"

[partition]: https://cwiki.apache.org/DIRxSRVx11/143-adding-your-own-partition-resp-suffix.html "Adding your own partition/suffix"

--------------------

** Theresa Henze, 2013 **