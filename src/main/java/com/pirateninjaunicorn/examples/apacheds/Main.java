package com.pirateninjaunicorn.examples.apacheds;

import java.io.File;
import java.net.URL;

import javax.naming.NamingEnumeration;
import javax.naming.directory.SearchResult;

import com.google.common.io.Resources;
import com.pirateninjaunicorn.examples.apacheds.client.LdapConnector;
import com.pirateninjaunicorn.examples.apacheds.server.EmbeddedLdapServer;

public class Main {

	private static final String HOST = "localhost";
	private static final Integer PORT = 10389;

	public static void main(final String[] args) {
		Main main = new Main();
		main.execute();
		System.exit(0);
	}

	private void execute() {

		// start server
		System.out.println("Initializing embedded LDAP server");
		EmbeddedLdapServer embeddedLdapServer = new EmbeddedLdapServer(HOST,
				PORT);
		try {
			embeddedLdapServer.start();

			// Load the directory as a resource
			URL url = Resources.getResource("ldap-example-data.ldif");
			embeddedLdapServer.applyLdif(new File(url.getFile()));
			System.out.println("Embedded LDAP server started");
			System.out.println();

			// wait for input
			System.out.print("Enter username to search LDAP for: ");
			String input = System.console().readLine();
			while (!"".equals(input) && !"x".equals(input)) {

				System.out.println("Searching for '" + input + "' ...");

				// let connector search for user
				LdapConnector connector = new LdapConnector(HOST,
						String.valueOf(PORT));
				NamingEnumeration<SearchResult> results = connector
						.searchDirectory(input);

				if (results.hasMore()) {
					System.out.println("Found:");
					while (results.hasMore()) {
						System.out.println("   " + results.next().getName());
					}
				}

				// wait for input
				System.out.println();
				System.out.print("Enter username to search LDAP for: ");
				input = System.console().readLine();
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				// stop server
				embeddedLdapServer.stop();
				System.out.println("Embedded LDAP server stopped");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
